# fix_laravel_migration
a script that fixes the `1071 Specified key was too long; max key length is 767 bytes migration` issue.

So if you create laravel projects often and you face that issue and everytime you forget how to solve it. just download this script and keep it handy.

## Installation
* `git clone https://github.com/lordadamson/fix_laravel_migration.git`
* `cd fix_laravel_migration`
* `sudo mv fixlaravelmigration.sh /usr/local/bin`

## Usage
Right after you create a new laravel project cd into it's root directory and run `fixlaravelmigration.sh` and then do your migration.
